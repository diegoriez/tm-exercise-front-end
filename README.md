# Today my music is ...

***Example using stateless components with React. Integration with D3js***

* DISCOGS API
* Standard ES6 modules using Babel + browserify
* React > 0.15
* Redux
* CSS - Modules
* POST - CSS
* Unit test with tape, ezyme, sinon (covered 89%)
* Storybook
* BrowserSync (livereload)
* D3js ( to calculate chart data )
* Adaptative design

## Scripts:
The `package.json` file comes with the following scripts

`npm run <name of script>`

* `devc` runs a dev console that reports lint and unit tests and start watching for changes at ./src/**/ *.js files.
* `dev` launch a dev server providing the app at build  (http://localhost:3000) serve alt ./build and start watching for changes at ./src/* files
* `test` run provided unit test
* `storybook` launch a server at port 9001
* `coverall` coverage report placed at ./coverage
* `dist` generates a distribution version, placed at ./dist
* `check` check outdated npm modules
* `update` update  outdated npm modules

## ToolBox:
used for implementation
* React
* Redux (to organize the application code)
* Browserify (bundling)
* Css-modulesify (A browserify plugin to load CSS - modules)
* Literalify a browserify transform for replacing require calls with arbitrary code
* Eslint (to ensure consistency in code)
* Tape (unit testing)
* Sinon (test spies)
* Enzyme (Test assertion helpers for use with React's shallowRender test utils)
* Babel (ES6 transpiler)
* Jade (html templating)
* POST-css 
* Browsersync (synchronised browser testing)
* Storybook  (develop and design React UI components without running the app)

## Demo:
[APP](https://today-my-music-is.herokuapp.com)

## Note
 you need an user token, to get your user collection from discogs. 
 this token should be configured in your server
* [Discogs Auth](https://www.discogs.com/developers/#page:authentication,header:authentication-request-token-url)

## Useful links:
* [D3 within React the right way](http://oli.me.uk/2015/09/09/d3-within-react-the-right-way/)
* [React Pure Component Starter](https://github.com/ericelliott/react-pure-component-starter)
* [Functional Stateless Components in React 0.14](https://medium.com/@joshblack/stateless-components-in-react-0-14-f9798f8b992d#.5ivkypjke)
* [React Storybook](https://github.com/kadirahq/react-storybook)

