var path = require('path');
var Discogs = require('disconnect').Client;

module.exports = function(app, ops) {

    app.get('/api/artist-releases/:id', function(req, res) {
        var id = req.params.id;
        var db = new Discogs({ userToken: ops.token }).database();
        db.getArtistReleases(id, { 'per_page': ops.perPage },
            function(err, data) {

                if (err) {
                    res.status(err.statusCode).send(err.message);
                    return;
                }

                res
                    .status(200)
                    .send({
                        id: id,
                        data: data
                    });
            }
        );
    });

    app.get('/api/user-collection', function(req, res) {

        var col = new Discogs({ userToken: ops.token }).user().collection();

        col.getReleases(ops.user, 0, {page: 1, per_page: 15},
            function(err, data) {

                if (err) {
                    res.status(err.statusCode).send(err.message);
                    return;
                }

                res
                    .status(200)
                    .send(data);
            }
        );
    });
};
