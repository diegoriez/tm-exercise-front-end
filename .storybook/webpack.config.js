const autoprefixer = require('autoprefixer');
const simpleVars = require('postcss-simple-vars');
var pxtorem = require('postcss-pxtorem');
const nested = require('postcss-nested');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
    module: {
        loaders: [{
                test: /\.css$/,
                loader: 'style!css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss-loader'
            }, {
                test: /\.css.global$/,
                loader: 'style!css-loader!postcss-loader'
            }]
    },
    postcss: function() {
        return [autoprefixer, simpleVars, pxtorem, nested];
    }
}
