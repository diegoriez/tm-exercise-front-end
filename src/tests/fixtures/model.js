import deepFreeze from "deep-freeze";

/* eslint quotes: ["error", "double"]*/

// user collection response
export const response = deepFreeze({
    "pagination": {
        "per_page": 50,
        "items": 2,
        "page": 1,
        "urls": {},
        "pages": 1
    },
    "releases": [{
        "instance_id": 180254796,
        "rating": 3,
        "basic_information": {
            "labels": [{
                "name": "Not On Label (Amy Winehouse)",
                "entity_type": "1",
                "catno": "AMYWINEHOUSE001",
                "resource_url": "https://api.discogs.com/labels/138733",
                "id": 138733,
                "entity_type_name": "Label"
            }],
            "formats": [{
                "descriptions": ["LP", "Compilation", "Limited Edition", "Unofficial Release"],
                "text": "Half Red, Half Pink",
                "name": "Vinyl",
                "qty": "1"
            }],
            "thumb": "https://api-img.discogs.com/ocBHgocujzEc05gqH31xCJQzeJQ=/fit-in/150x150/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-3030051-1312468371.jpeg.jpg",
            "title": "The Best Of Amy Winehouse",
            "artists": [{
                "join": "",
                "name": "Amy Winehouse",
                "anv": "",
                "tracks": "",
                "role": "",
                "resource_url": "https://api.discogs.com/artists/151223",
                "id": 151223
            }],
            "resource_url": "https://api.discogs.com/releases/3030051",
            "year": 2011,
            "id": 3030051
        },
        "folder_id": 1,
        "date_added": "2016-07-18T10:55:22-07:00",
        "id": 3030051
    },

        {
            "basic_information": {
                "artists": [{
                    "anv": "",
                    "id": 4232982,
                    "join": ",",
                    "name": "J. Albert",
                    "resource_url": "https://api.discogs.com/artists/4232982",
                    "role": "",
                    "tracks": ""
                }, {
                    "anv": "",
                    "id": 3787553,
                    "join": ",",
                    "name": "Person Of Interest",
                    "resource_url": "https://api.discogs.com/artists/3787553",
                    "role": "",
                    "tracks": ""
                }],
                "formats": [{
                    "descriptions": [
                        "12\""
                    ],
                    "name": "Vinyl",
                    "qty": "1"
                }],
                "id": 8386456,
                "labels": [{
                    "catno": "EDR002",
                    "entity_type": "1",
                    "entity_type_name": "Label",
                    "id": 832089,
                    "name": "Exotic Dance Records",
                    "resource_url": "https://api.discogs.com/labels/832089"
                }],
                "resource_url": "https://api.discogs.com/releases/8386456",
                "thumb": "https://api-img.discogs.com/qqT8aJMkJnFpWXWMI_EKrM1UQXI=/fit-in/150x150/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-8386456-1463944669-1872.jpeg.jpg",
                "title": "EDR002",
                "year": 2016
            },
            "date_added": "2016-07-23T16:57:30-07:00",
            "folder_id": 1,
            "id": 8386456,
            "instance_id": 180872349,
            "rating": 1
        }

    ]
});

// releases by artist response
export const responseArtistReleases = deepFreeze({

    "data": {
        "pagination": {
            "items": 635,
            "page": 1,
            "pages": 43,
            "per_page": 15,
            "urls": {
                "last": "https://api.discogs.com/artists/151223/releases?per_page=15&page=43",
                "next": "https://api.discogs.com/artists/151223/releases?per_page=15&page=2"
            }
        },
        "releases": [{
            "artist": "Amy Winehouse",
            "id": 51290,
            "main_release": 716019,
            "resource_url": "https://api.discogs.com/masters/51290",
            "role": "Main",
            "thumb": "https://api-img.discogs.com/mrKMH4Hf3WulwCbhjhvYnPSI_1Q=/fit-in/90x90/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-716019-1431410864-5661.jpeg.jpg",
            "title": "Frank",
            "type": "master",
            "year": 2003
        }]
    },
    "id": "151223"
});

// reduced model ( by USER COLLECTION )
export const userCollectionModel = deepFreeze({
    releaseType: "by-user-collection",
    data: (response.releases.slice(0)).sort((a, b) => a.rating - b.rating).reverse()
});

// reduced model ( by ARTIST RELEASE )
export const artistReleasesModel = deepFreeze({
    releaseType: "by-artist-release",
    data: responseArtistReleases.data.releases
});

// -----------------------------------------------------------
//    STATE - MODEL
// -----------------------------------------------------------

// redux STATE-model with user collection
export const stateModel = deepFreeze({
    headerTitle: "my App",
    fetchData: false,
    releaseData: Object.assign({}, userCollectionModel),
    ratingSelected: 3,
    artistSelected: {
        id: "",
        name: ""
    },
    bubblesContainer: {
        dimension: 600,
        viewBoxDimensions: { w: 500, h: 600 },
        scaleTipFactor: []
    }
});

// redux STATE-model with artist release
export const stateModelArtistRelease = deepFreeze({
    headerTitle: "my App",
    fetchData: false,
    releaseData: Object.assign({}, artistReleasesModel),
    ratingSelected: 3,
    artistSelected: {
        id: 151223,
        name: "Amy Winehouse"
    },
    bubblesContainer: {
        dimension: 600,
        viewBoxDimensions: { w: 500, h: 600 },
        scaleTipFactor: []
    }
});
