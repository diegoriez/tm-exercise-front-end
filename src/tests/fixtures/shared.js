 import {
     stateModel
 } from 'tests/fixtures/model';

 export const TYPES = {
     HEADER_TITLE: 'header-title',
     FETCH_DATA: 'fetch-collection',
     USER_COLLECTION: 'by-user-collection',
     ARTIST_RELEASE: 'by-artist-release',
     RATING_SELECTED: 'rating-selected',
     ARTIST_SELECTED: 'artist-selected',
     BUBBLE_CONTAINER_DIMENSIONS: 'bubble-container-dimensions'
 };


 export const bubbleNodes = [{
     id: 123,
     name: 'Amy',
     value: 3,
     x: 10,
     y: 5,
     r: 20
 }, {
     id: 456,
     name: 'Parov',
     value: 3,
     x: 12,
     y: 5,
     r: 10
 }, {
     id: 789,
     name: 'Joris',
     value: 4,
     x: 10,
     y: 5,
     r: 5
 }];

 // ---------------------
 //  DOM helpers
 // ---------------------

 export const nodeData = [{ value: 5 }, { value: 3 }];

 export const getBubblesNodesReturn = [
     { r: 168.77121271436351, value: 5, x: 168.77121271436354, y: 300 },
     { r: 130.72961923239185, value: 3, x: 469.2703807676081, y: 300 }
 ];

 export const innerhtml = `<div id="root">
                        <div class="container">
                          <div>
                            <svg>
                              <g id="bubbles-container">
                                <g>
                                  <g class="bubble-tip">
                                  </g>
                                </g>
                              </g>
                            </svg>
                          </div>
                        </div>
                    </div>`;



 export const viewBoxDimensions = { w: 0, h: 0 };


 // -----------------------
 // for bubble container
 // -----------------------
 export const bubbleNode = [{
     r: 49.41709084030851,
     value: 5,
     x: 151.22809127878003,
     y: 160.8975327478884
 }];

 export const dataForUserCollectionBubblesNodes = {
     releases: [{
         value: 3,
         name: 'The Best Of Amy Winehouse',
         id: 3030051,
         x: 151.22809127878003,
         y: 160.8975327478884,
         r: 49.41709084030851
     }]
 };

 export const dataFoAtistsBubblesNodes = {
     releases: [{
         value: 2003,
         name: 'Frank',
         id: 51290,
         x: 151.22809127878003,
         y: 160.8975327478884,
         r: 49.41709084030851
     }]
 };

 export const fakeFilterByRating = () => [stateModel.releaseData.data[0]];
 export const fakeGetBubbleNodes = () => bubbleNode;

 // -------------------------------
 // ARTIST LIST
 // ------------------------------

 // user collection list container
 export const mockForUserCollectiontListContainer = [{
     displayArtistLink: true,
     id: 151223,
     name: 'Amy Winehouse',
     picUrl: 'https://api-img.discogs.com/ocBHgocujzEc05gqH31xCJQzeJQ=/fit-in/150x150/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-3030051-1312468371.jpeg.jpg',
     title: 'The Best Of Amy Winehouse',
     added: '2016-07-18',
     rating: 3
 }];

 // artist release list list container
 export const mockForArtistReleasesListContainer = [{
     displayArtistLink: false,
     id: 51290,
     name: 'Amy Winehouse',
     picUrl: 'https://api-img.discogs.com/mrKMH4Hf3WulwCbhjhvYnPSI_1Q=/fit-in/90x90/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-716019-1431410864-5661.jpeg.jpg',
     title: 'Frank',
     added: 2003,
     rating: 0
 }];

 // artist release list component
 export const mockDataForArtistListUserCollection = [{
     displayArtistLink: true,
     picUrl: 'http://...',
     id: 151223,
     name: 'Amy Winehouse',
     title: 'super artist',
     added: '25/11/69',
     rating: 5,
 }, {
     displayArtistLink: false,
     id: 4232982,
     name: 'J. Albert',
     picUrl: 'http://...',
     title: 'artist',
     added: '10/11/69',
     rating: 5,
 }];

 export const fakeReactComp = React => () => (React.createElement('div', null));
 export const fakeStore = () => ({
     getState: () => stateModel
 });
