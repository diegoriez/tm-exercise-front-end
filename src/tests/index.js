 import './scripts/actions';
 import './scripts/constants';
 import './scripts/helpers/dom';
 import './scripts/helpers/fetch';
 import './scripts/helpers/helper';
 import './scripts/store';
 import './scripts/reducers';

 import './scripts/components/artists-list';
 import './scripts/components/artist-release';
 import './scripts/components/header';
 import './scripts/components/bubble-chart';
 import './scripts/components/rating-selection';
 import './scripts/components/app';

 import './scripts/containers/artists-list';
 import './scripts/containers/header';
 import './scripts/containers/artist-release';
 import './scripts/containers/bubble-chart';
 import './scripts/containers/rating-selection';
 import './scripts/containers/app';
