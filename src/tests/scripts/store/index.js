import test from 'tape';
import store from 'scripts/store';

const before = test;
const after = test;

before('description: store', t => {
    t.end();
});

test('store', t => {
    const s = store({ k: 1 }, () => () => 'fake');
    const actual = Object.keys(s),
        expect = ['dispatch', 'subscribe', 'getState', 'replaceReducer'];
    t.deepEqual(actual, expect);
    t.end();
});

after('end test ---------------------------------------', t => {
    t.end();
});
