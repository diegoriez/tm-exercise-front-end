import test from 'tape';
import mock from 'mock';
import React from 'react';

const $ = React.createElement;

const ratingSelection = mock('scripts/containers/rating-selection', {
    'scripts/components/rating-selection': () => ($('div', null))
}, require).default;

const createrRatingSelection = ratingSelection(React);

const before = test;
const after = test;


before('description: container rating-selection', t => {
    t.end();
});

test('should return Connect function', t => {
    const actual = createrRatingSelection.name === 'Connect',
        expect = true;
    t.ok(actual, expect);
    t.end();
});

after('end test ---------------------------------------', t => {
    t.end();
});
