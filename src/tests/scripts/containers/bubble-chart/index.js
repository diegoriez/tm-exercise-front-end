import test from 'tape';
import mock from 'mock';
import React from 'react';
import {
    stateModel,
    stateModelArtistRelease
} from 'tests/fixtures/model';

import {
    dataForUserCollectionBubblesNodes,
    dataFoAtistsBubblesNodes,
    bubbleNode,
    fakeFilterByRating,
    fakeGetBubbleNodes,
    fakeStore,
    TYPES
} from 'tests/fixtures/shared';

const $ = React.createElement;

const bubbleChart = mock('scripts/containers/bubble-chart', {
    'scripts/components/bubble-chart': ({ title, }) => ($('div', null, title)),
    'scripts/helpers/dom': {
        getBubblesNodes: fakeGetBubbleNodes
    },
    'scripts/constants': {
        USER_COLLECTION: TYPES.USER_COLLECTION
    },
    'scripts': {
        store: fakeStore
    },
    'scripts/helpers/helper': {
        cropText: val => val
    }
}, require);


const before = test;
const after = test;

before('description: container bubble chart', t => {
    t.end();
});

test('getValuesForNodes: user collection', t => {
    const data = stateModel.releaseData.data,
        actual = bubbleChart.getValuesForNodes(data, 'rating'),
        expect = [{ value: 4 }, { value: 2 }];
    t.deepEqual(actual, expect);
    t.end();
});

test('getValuesForNodes: artist release ', t => {
    const data = stateModelArtistRelease.releaseData.data,
        actual = bubbleChart.getValuesForNodes(data, 'year'),
        expect = [{ value: 2004 }];
    t.deepEqual(actual, expect);
    t.end();
});

test('filterUserCollectionByRating', t => {
    const data = stateModel.releaseData.data,
        actual = bubbleChart.filterUserCollectionByRating(data, 3),
        expect = [data[0]];
    t.deepEqual(actual, expect);
    t.end();
});

test('collectionByUserBubbles', t => {
    const data = stateModel.releaseData.data,
        actual = bubbleChart.collectionByUserBubbles(data, bubbleNode, fakeFilterByRating),
        expect = dataForUserCollectionBubblesNodes.releases;
    t.deepEqual(actual, expect);
    t.end();
});

test('releasesByArtistBubbles', t => {
    const data = stateModelArtistRelease.releaseData.data,
        actual = bubbleChart.releasesByArtistBubbles(data, bubbleNode),
        expect = dataFoAtistsBubblesNodes.releases;
    t.deepEqual(actual, expect);
    t.end();
});

test('doBubbles by user collection', t => {
    const actual = bubbleChart.doBubbles(stateModel),
        expect = dataForUserCollectionBubblesNodes.releases;
    t.deepEqual(actual, expect);
    t.end();
});

test('doBubbles by artist releases', t => {
    const actual = bubbleChart.doBubbles(stateModelArtistRelease),
        expect = dataFoAtistsBubblesNodes.releases;
    t.deepEqual(actual, expect);
    t.end();
});

test('getReleaseArtistList', t => {
    const actual = bubbleChart.getReleaseArtistList(stateModel.releaseData.data, 3030051),
        expect = [{ id: 151223, name: 'Amy Winehouse' }];
    t.deepEqual(actual, expect);
    t.end();
});

test('should return Connect function', t => {
    const createBubbleChart = bubbleChart.bubbleChartContainer(React),
        actual = createBubbleChart.name === 'Connect',
        expect = true;
    t.ok(actual, expect);
    t.end();
});

after('end test ---------------------------------------', t => {
    t.end();
});
