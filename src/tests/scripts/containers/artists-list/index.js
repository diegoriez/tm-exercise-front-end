import test from 'tape';
import mock from 'mock';
import React from 'react';
import { stateModel, stateModelArtistRelease } from 'tests/fixtures/model';
import {
    TYPES,
    mockForUserCollectiontListContainer,
    mockForArtistReleasesListContainer
} from 'tests/fixtures/shared';

const $ = React.createElement;

const artistsList = mock('scripts/containers/artists-list', {
    'scripts/components/artists-list': () => ($('div', null)),
    'scripts/constants': {
        USER_COLLECTION: TYPES.USER_COLLECTION
    }
}, require);

const createArtistsList = artistsList.artitsListContainter(React);


const before = test;
const after = test;

before('description: container artist list', t => {
    t.end();
});

test('prepareDataForUserCollectionList', t => {
    const actual = artistsList.prepareDataForUserCollectionList(stateModel.releaseData.data, 3)[0],
        expect = mockForUserCollectiontListContainer[0];
    t.deepEqual(actual, expect);
    t.end();
});

test('prepareDataForArtistReleaseList', t => {
    const actual = artistsList.prepareDataForArtistReleaseList(stateModelArtistRelease.releaseData.data)[0],
        expect = mockForArtistReleasesListContainer[0];
    t.deepEqual(actual, expect);
    t.end();
});

test('preareData', t => {

    let actual, expect;

    actual = artistsList.preareData(TYPES.USER_COLLECTION).name;
    expect = 'prepareDataForUserCollectionList';

    t.equal(actual, expect);

    actual = artistsList.preareData(TYPES.ARTIST_RELEASE).name;
    expect = 'prepareDataForArtistReleaseList';
    t.equal(actual, expect);

    t.end();
});

test('should return Connect function', t => {
    const actual = createArtistsList.name === 'Connect',
        expect = true;
    t.ok(actual, expect);
    t.end();
});

after('end test ---------------------------------------', t => {
    t.end();
});
