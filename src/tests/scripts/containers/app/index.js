import test from 'tape';
import mock from 'mock';
import React from 'react';
import { TYPES } from 'tests/fixtures/shared';

const $ = React.createElement;

const app = mock('scripts/containers/artist-release', {
    'scripts/components/app': () => ($('div', null)),
    'scripts/constants': {
        USER_COLLECTION: TYPES.USER_COLLECTION
    },
    'scripts/components/artist-release/styles.css': {
        title: 'title'
    }
}, require).default;

const createArtistRelease = app(React);

const before = test;
const after = test;

before('description: container app', t => {
    t.end();
});

test('should return Connect function', t => {
    const actual = createArtistRelease.name === 'Connect',
        expect = true;
    t.ok(actual, expect);
    t.end();
});

after('end test ---------------------------------------', t => {
    t.end();
});
