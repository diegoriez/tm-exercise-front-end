import test from 'tape';
import mock from 'mock';
import React from 'react';

const $ = React.createElement;

const artistRelease = mock('scripts/containers/artist-release', {
    'scripts/components/artist-release': () => ($('div', null))
}, require).default;

const createArtistRelease = artistRelease(React);

const before = test;
const after = test;


before('description: container artist release', t => {
    t.end();
});

test('should return Connect function', t => {
    const actual = createArtistRelease.name === 'Connect',
        expect = true;
    t.ok(actual, expect);
    t.end();
});

after('end test ---------------------------------------', t => {
    t.end();
});
