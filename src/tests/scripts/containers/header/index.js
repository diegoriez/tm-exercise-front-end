import test from 'tape';
import mock from 'mock';
import React from 'react';

const $ = React.createElement;

const header = mock('scripts/containers/header', {
    'scripts/components/header': ({ title }) => ($('div', null, title))
}, require).default;

const createHeader = header(React);

const before = test;
const after = test;

before('description: container header', t => {
    t.end();
});

test('should return Connect function', t => {
    const actual = createHeader.name === 'Connect',
        expect = true;
    t.ok(actual, expect);
    t.end();
});

after('end test ---------------------------------------', t => {
    t.end();
});
