import test from 'tape';
import mock from 'mock';
import { TYPES } from 'tests/fixtures/shared';
import {
    response,
    responseArtistReleases,
    userCollectionModel,
    artistReleasesModel
} from 'tests/fixtures/model';

const before = test;
const after = test;

const reducers = mock('scripts/reducers', {
    'scripts/constants': {
        'HEADER_TITLE': TYPES.HEADER_TITLE,
        'FETCH_DATA': TYPES.FETCH_DATA,
        'USER_COLLECTION': TYPES.USER_COLLECTION,
        'ARTIST_RELEASE': TYPES.ARTIST_RELEASE,
        'RATING_SELECTED': TYPES.RATING_SELECTED,
        'ARTIST_SELECTED': TYPES.ARTIST_SELECTED,
        'BUBBLE_CONTAINER_DIMENSIONS': TYPES.BUBBLE_CONTAINER_DIMENSIONS
    }
}, require);

before('description: reducers', t => {
    t.end();
});

test('headerTitle', t => {
    const actual = reducers.headerTitle('', {
            type: TYPES.HEADER_TITLE,
            payload: 'my app'
        }),
        expect = 'my app';
    t.equal(actual, expect);
    t.end();
});

test('fetchData', t => {
    const actual = reducers.fetchData(true, {
            type: TYPES.FETCH_DATA,
            payload: false
        }),
        expect = false;
    t.equal(actual, expect);
    t.end();
});

test('releaseData, by user collection', t => {
    const actual = reducers.releaseData({}, {
            type: TYPES.USER_COLLECTION,
            payload: response
        }),
        expect = userCollectionModel;
        //
    t.deepEqual(actual, expect);
    t.end();
});

test('releaseData, by artist releases', t => {
    const actual = reducers.releaseData({}, {
            type: TYPES.ARTIST_RELEASE,
            payload: responseArtistReleases
        }),
        expect = artistReleasesModel;

      // userCollectionModel.data[0].rating



    t.deepEqual(actual, expect);
    t.end();
});

test('ratingSelected', t => {
    const actual = reducers.ratingSelected(0, {
            type: TYPES.RATING_SELECTED,
            payload: 5
        }),
        expect = 5;
    t.equal(actual, expect);
    t.end();
});

test('artistSelected', t => {
    const actual = reducers.artistSelected({}, {
            type: TYPES.ARTIST_SELECTED,
            payload: {
                id: 124,
                name: 'Amy'
            }
        }),
        expect = {
            id: 124,
            name: 'Amy'
        };
    t.deepEqual(actual, expect);
    t.end();
});

test('bubblesContainer', t => {
    const actual = reducers.bubblesContainer({}, {
            type: TYPES.BUBBLE_CONTAINER_DIMENSIONS,
            payload: {
                dimension: 600,
                viewBoxDimensions: { w: 500, h: 600 },
                scaleTipFactor: 1
            }
        }),
        expect = {
            dimension: 600,
            viewBoxDimensions: { w: 500, h: 600 },
            scaleTipFactor: 1
        };
    t.deepEqual(actual, expect);
    t.end();
});

test('rootReducer', t => {
    const actual = reducers.rootReducer.name,
        expect = 'combination';
    t.equal(actual, expect);
    t.end();
});

after('end test ---------------------------------------', t => {
    t.end();
});
