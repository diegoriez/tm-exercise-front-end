import test from 'tape';
import sinon from 'sinon';
import React from 'react';
import mock from 'mock';
import { shallow } from 'enzyme';
import { mockDataForArtistListUserCollection } from 'tests/fixtures/shared';

const $ = React.createElement;

const artistsList = mock('scripts/components/artists-list', {
    'scripts/components/artists-list/styles.css': {
        pic: 'pic',
        'artists': 'artists',
        'artistMetadata': 'artistMetadata',
        'pointer': 'pointer'

    }
}, require).default;

const createArtistsList = artistsList(React);

const before = test;
const after = test;

const component = shallow($(createArtistsList, { list: mockDataForArtistListUserCollection }));


before('description: component artists list', t => {
    t.end();
});

test('should render correctly', t => {
    const actual = component.type(),
        expect = 'ul';
    t.equal(actual, expect);
    t.end();
});

test('should have 2 children', t => {
    const actual = component.find('ul').children().length,
        expect = 2;
    t.equal(actual, expect);
    t.end();
});

test('<li> element is shown as a link', t => {
    const actual = component.find('ul').childAt(0).props().className,
        expect = 'pointer';
    t.equal(actual, expect);
    t.end();
});

test('<li> dataset', t => {

    let actual, expect;

    actual = component.find('ul').childAt(0).props()['data-artist-id'];
    expect = 151223;
    t.equal(actual, expect);

    actual = component.find('ul').childAt(0).props()['data-artist-name'];
    expect = 'Amy Winehouse';
    t.equal(actual, expect);

    t.end();
});

test('should have background pic ', t => {
    const actual = component.childAt(0).find('.pic').props().style,
        expect = { 'backgroundImage': `url(${'"http://..."'})` };
    t.deepEqual(actual, expect);
    t.end();
});

test('artist name', t => {

    let actual, expect;

    actual = component.childAt(1).find('.artists').childAt(0).text();
    expect = 'J. Albert';
    t.deepEqual(actual, expect, 'artist name');

    t.end();
});

test('should have aritst title', t => {
    const actual = component.childAt(0)
        .find('.artists')
        .children()
        .find('h4')
        .text(),

        expect = 'super artist';
    t.deepEqual(actual, expect);
    t.end();
});

test('artist metadata should have 5 stars', t => {
    const actual = component.childAt(0)
        .find('.artistMetadata')
        .find('i')
        .length,

        expect = 5;
    t.deepEqual(actual, expect);
    t.end();
});

test('should have date added', t => {
    const actual = component.childAt(0)
        .find('.artistMetadata')
        .find('time').length,

        expect = 1;
    t.deepEqual(actual, expect);
    t.end();
});

test('picture has class pic', t => {

    const actual = component.childAt(0).find('.pic').length,
        expect = 1;

    t.deepEqual(actual, expect);
    t.end();
});


test('artistsClick', t => {

    const callback = sinon.spy(),
        props = {
            list: mockDataForArtistListUserCollection.slice(0),
            artistClickListener: callback
        };

    shallow($(createArtistsList, props))
        .find('ul')
        .childAt(0)
        .simulate('click');

    const actual = callback.calledOnce,
        expect = true;
    t.equal(actual, expect, 'event was called once');

    t.end();
});

after('end test ---------------------------------------', t => {
    t.end();
});
