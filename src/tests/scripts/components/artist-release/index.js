import test from 'tape';
import React from 'react';
import mock from 'mock';
import sinon from 'sinon';
import { shallow } from 'enzyme';

const artistRelease = mock('scripts/components/artist-release', {
    'scripts/components/artist-release/styles.css': {
        container: 'container'
    }
}, require).default;

const $ = React.createElement;
const createArtistRelease = artistRelease(React);

const before = test;
const after = test;

const callback = sinon.spy();
const props = {
    clickListener: callback,
    title: 'Amy'
};

before('description: component artist-release', t => {
    t.end();
});

test('should render correctly', t => {
    const actual = shallow($(createArtistRelease, props)).type(),
        expect = 'div';
    t.equal(actual, expect);
    t.end();
});

test('should have 2 children', t => {
    const actual = shallow($(createArtistRelease, props)).children().length,
        expect = 2;
    t.equal(actual, expect);
    t.end();
});

test('title should be defined', t => {
    const actual = shallow($(createArtistRelease, props)).find('p').text('Amy'),
        expect = 'Amy';
    t.equal(actual, expect);
    t.end();
});

test('selectByRatingListener', t => {

    shallow($(createArtistRelease, props)).find('button').simulate('click');

    const actual = callback.calledOnce,
        expect = true;
    t.equal(actual, expect, 'event was called once');
    t.end();
});


after('end test ---------------------------------------', t => {
    t.end();
});
