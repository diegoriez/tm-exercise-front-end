import test from 'tape';
import React from 'react';
import mock from 'mock';
import { shallow } from 'enzyme';
import { fakeReactComp } from 'tests/fixtures/shared';

const $ = React.createElement;

const App = mock('scripts/components/app', {
    'scripts/containers/header': fakeReactComp,
    'scripts/containers/bubble-chart': {
        bubbleChartContainer: fakeReactComp
    },
    'scripts/containers/artist-release': () => () => $('div', { className: 'artist-release' }),
    'scripts/containers/rating-selection': () => () => $('div', { className: 'rating-selection' }),
    'scripts/containers/artists-list': {
        artitsListContainter: fakeReactComp
    }

}, require).default;

const app = shallow($(App(React), { byUserCollection: true }));
const before = test;
const after = test;

before('description: App component', t => {
    t.end();
});

test('App', t => {
    const actual = app.type(),
        expect = 'div';
    t.equal(actual, expect, 'should render correctly');

    t.end();
});

test('switch comp', t => {

    let actual, expect;

    actual = app.find('.container').childAt(2).html();
    expect = '<div class="rating-selection"></div>';
    t.equal(actual, expect);

    actual = shallow($(App(React), { byUserCollection: false })).find('.container').childAt(2).html();
    expect = '<div class="artist-release"></div>';
    t.equal(actual, expect);

    t.end();
});



after('end test ---------------------------------------', t => {
    t.end();
});
