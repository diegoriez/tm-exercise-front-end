import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import mock from 'mock';
import { shallow } from 'enzyme';
import { bubbleNodes, foo } from 'tests/fixtures/shared';


const bubbleChart = mock('scripts/components/bubble-chart', {
    'scripts/components/bubble-chart/styles.css': {
        container: 'container',
        text: 'text',
        bubbleCenter: 'bubbleCenter',
        oneStar: 'oneStar',
        twoStars: 'twoStars',
        threeStars: 'threeStars',
        fourStars: 'fourStars',
        fiveStars: 'fiveStars',
        starOutline: 'starOutline',
        starFilled: 'starFilled',
        pointer: 'pointer'
    }
}, require).default;

const $ = React.createElement;
const createBubbleChart = bubbleChart(React);

const before = test;
const after = test;

const props = {
    w: 968,
    h: 980,
    bubbles: bubbleNodes,
    bubbleClickListener: foo,
    scaleTip: [],
    createStars: true
};

const bubblesChart = shallow($(createBubbleChart, props));
const bubblesContainer = bubblesChart.find('#bubbles-container');

before('description: component bubble-chart', t => {
    t.end();
});

test('should render correctly', t => {
    const actual = bubblesChart.type(),
        expect = 'div';
    t.equal(actual, expect);
    t.end();
});

test('svg viewBox property is setted ', t => {
    const actual = bubblesChart.find('svg').props().viewBox,
        expect = '0,0,968,980';
    t.equal(actual, expect);
    t.end();
});

test('.bubbles-container is defined', t => {
    const actual = bubblesContainer.length,
        expect = 1;
    t.equal(actual, expect);
    t.end();
});

test('svg have 3 groups children ', t => {
    const actual = bubblesContainer.children().length,
        expect = 3;
    t.equal(actual, expect);
    t.end();
});

test('<g> transform prop', t => {
    const actual = bubblesContainer.childAt(0).props().transform,
        expect = 'translate(10,5)';
    t.equal(actual, expect);
    t.end();
});

test('<g> id prop', t => {
    const actual = bubblesContainer.childAt(0).props().id,
        expect = 123;
    t.equal(actual, expect);
    t.end();
});

test('each <g> group have 4 circle children ', t => {
    const actual = bubblesContainer.find('g').find('circle').length,
        expect = 12;
    t.equal(actual, expect);
    t.end();
});

test('circles radious are defined', t => {

    const gElm = bubblesContainer.childAt(0);

    let actual, expect;

    actual = gElm.childAt(0).props().r;
    expect = 20;
    t.equal(actual, expect);

    actual = gElm.childAt(1).props().r;
    expect = 10;
    t.equal(actual, expect);

    actual = gElm.childAt(2).props().r;
    expect = 1;
    t.equal(actual, expect);

    actual = gElm.childAt(3).props().r;
    expect = 18;
    t.equal(actual, expect);

    t.end();
});

test('circle style color by rating', t => {

    const gElm = bubblesContainer.childAt(0),

        actual = gElm.childAt(1).props().className.search('threeStars') !== -1,
        expect = true;

    t.ok(actual, expect);
    t.end();
});


test('<g> should have className pointer', t => {
    const actual = bubblesContainer.childAt(0).props().className,
        expect = 'pointer';
    t.equal(actual, expect);
    t.end();
});

test('clickListener', t => {

    const callback = sinon.spy(),
        properties = Object.assign({}, props, { clickListener: callback });

    shallow($(createBubbleChart, properties))
        .find('#bubbles-container').childAt(0).simulate('click');

    const actual = callback.calledOnce,
        expect = true;
    t.equal(actual, expect, 'event was called once');
    t.end();
});

test('bubble metadata container is defined', t => {
    const actual = bubblesContainer.childAt(0).find('.bubble-tip').length,
        expect = 1;
    t.equal(actual, expect);
    t.end();
});

test('bubble metadata container', t => {
    const actual = bubblesContainer.childAt(0)
        .find('.bubble-tip').props().transform,
        expect = 'scale(1) translate(0, -20)';
    t.equal(actual, expect);
    t.end();
});

test('star group should have five elements', t => {
    const actual = bubblesContainer.childAt(0)
        .find('.bubble-tip').childAt(0).children().length,
        expect = 5;

    t.equal(actual, expect);
    t.end();
});

test('star group should have one elements', t => {

    const actual = shallow($(createBubbleChart,
            Object.assign({}, props, { createStars: false })))
        .find('#bubbles-container')
        .childAt(0)
        .find('.bubble-tip')
        .childAt(0)
        .children()
        .length,

        expect = 1;

    t.equal(actual, expect);
    t.end();
});

test('three stars are filled', t => {
    const actual = bubblesContainer.childAt(0)
        .find('.bubble-tip').childAt(0).childAt(2).props().className,
        expect = 'starFilled';
    t.deepEqual(actual, expect);
    t.end();
});

test('<text> children is defined', t => {

    const actual = bubblesContainer.childAt(0)
        .find('.bubble-tip').find('text').text(),
        expect = 'Amy';
    t.equal(actual, expect);
    t.end();

});

after('end test ---------------------------------------', t => {
    t.end();
});
