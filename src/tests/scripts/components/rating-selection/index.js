import test from 'tape';
import mock from 'mock';
import React from 'react';
import sinon from 'sinon';
import { shallow } from 'enzyme';
// import ratingSelection from 'scripts/components/rating-selection';

const ratingSelection = mock('scripts/components/rating-selection', {
    'scripts/components/rating-selection/styles.css': {
        isSelected: 'isSelected',
        ratingSelection: 'ratingSelection',
        ratingSelectionFirstElm: 'ratingSelectionFirstElm'
    }
}, require).default;

const $ = React.createElement;
const createRatingSelection = ratingSelection(React);

const before = test;
const after = test;

const callback = sinon.spy();

const props = {
    selected: '1',
    clickListener: callback
};

before('description: component rating-selection', t => {
    t.end();
});

test('should render correctly', t => {
    const actual = shallow($(createRatingSelection, props)).type(),
        expect = 'div';
    t.equal(actual, expect);
    t.end();
});

test('should have 6 children', t => {
    const actual = shallow($(createRatingSelection, props)).find('button').length,
        expect = 6;
    t.equal(actual, expect);
    t.end();
});

test('is selected', t => {

    let actual, expect;

    actual = shallow($(createRatingSelection, props)).childAt(1).props().className;
    expect = 'isSelected';
    t.equal(actual, expect);

    t.end();
});

test('clickListener', t => {

    shallow($(createRatingSelection, props)).find('div').childAt(1).simulate('click');

    const actual = callback.calledOnce,
        expect = true;
    t.equal(actual, expect, 'event was called once');
    t.end();
});

after('end test ---------------------------------------', t => {
    t.end();
});
