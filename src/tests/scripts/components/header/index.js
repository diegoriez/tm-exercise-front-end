import test from 'tape';
import React from 'react';
import { shallow } from 'enzyme';
import header from 'scripts/components/header';

const $ = React.createElement;
const createHeader = header(React);

const before = test;
const after = test;

before('description: component header', t => {
    t.end();
});

test('should render correctly', t => {
    const actual = shallow($(createHeader, { title: '' })).type() === 'header',
        expect = true;
    t.ok(actual, expect);
    t.end();
});

test('title is defined', t => {
    const actual = shallow($(createHeader, { title: 'Today my music is...' })).find('h2').text(),
        expect = 'Today my music is...';
    t.deepEqual(actual, expect);
    t.end();
});

after('end test ---------------------------------------', t => {
    t.end();
});
