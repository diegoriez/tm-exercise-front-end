import test from 'blue-tape';
import mock from 'mock';
import {
    response
} from 'tests/fixtures/model';

const before = test;
const after = test;
const helpers = mock('scripts/helpers/fetch', {
    'isomorphic-fetch': () => Promise.resolve({ json: () => response })
}, require);

before('description: helpers fetchData', t => {
    t.end();
});

test('fetchData', t => {
    return helpers.fetchData('/disc')
        .then(resp => {
            const actual = resp,
                expect = response;
            t.deepEqual(actual, expect);
        });
});

after('end test ---------------------------------------', t => {
    t.end();
});
