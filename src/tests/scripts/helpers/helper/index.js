import test from 'tape';
import { cropText } from 'scripts/helpers/helper';

const before = test;
const after = test;

before('description:  helpers/helper', t => {
    t.end();
});

test('cropText', t => {
    const actual = cropText('averylongstringpcutit', 6),
        expect = 'averyl...';
    t.equal(actual, expect);
    t.end();
});

after('end test ---------------------------------------', t => {
    t.end();
});
