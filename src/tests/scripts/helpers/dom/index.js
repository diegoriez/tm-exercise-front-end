import test from 'tape';
import benv from 'benv';
import {
    getBubbleContainerDimensions,
    getViewBoxDimensions,
    getBubblesNodes,
    getBubblesTipsScaleFactor
} from 'scripts/helpers/dom';
import {
    nodeData,
    innerhtml,
    viewBoxDimensions,
    getBubblesNodesReturn
} from 'tests/fixtures/shared';

const before = test;
const after = test;

before('description: dom helpers', t => {

    benv.setup(() => {
        window.document.querySelector('body').innerHTML = innerhtml;
        t.end();
    });

});

test('getBubbleContainerDimensions', t => {
    const actual = getBubbleContainerDimensions(),
        expect = 0;
    t.equal(actual, expect);
    t.end();
});

test('getViewBoxDimensions', t => {
    const actual = getViewBoxDimensions(),
        expect = viewBoxDimensions;
    t.deepEqual(actual, expect);
    t.end();
});

test('getBubblesNodes', t => {
    const actual = getBubblesNodes(nodeData, 600),
        expect = getBubblesNodesReturn;
    t.deepEqual(actual, expect);
    t.end();
});

test('getBubblesTipsScaleFactor', t => {
    const actual = getBubblesTipsScaleFactor(),
        expect = [1];
    t.deepEqual(actual, expect);
    t.end();
});

after('end test ---------------------------------------', t => {
    benv.teardown();
    t.end();
});
