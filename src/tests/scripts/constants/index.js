import test from 'tape';
import * as TYPES from 'scripts/constants';

const before = test;
const after = test;

before('description: constants', t => {
    t.end();
});

test('HEADER_TITLE', t => {
    const actual = TYPES.HEADER_TITLE === 'header.title',
        expect = true;
    t.ok(actual, expect);
    t.end();
});

test('FETCH_DATA', t => {
    const actual = TYPES.FETCH_DATA === 'fetch.data',
        expect = true;
    t.ok(actual, expect);
    t.end();
});

test('USER_COLLECTION', t => {
    const actual = TYPES.USER_COLLECTION === 'by.user.collection',
        expect = true;
    t.ok(actual, expect);
    t.end();
});

test('ARTIST_RELEASE', t => {
    const actual = TYPES.ARTIST_RELEASE === 'by.artist.release',
        expect = true;
    t.ok(actual, expect);
    t.end();
});

test('ARTIST_SELECTED', t => {
    const actual = TYPES.ARTIST_SELECTED === 'artist.selected',
        expect = true;
    t.ok(actual, expect);
    t.end();
});

test('RATING_SELECTED', t => {
    const actual = TYPES.RATING_SELECTED === 'rating.selected',
        expect = true;
    t.ok(actual, expect);
    t.end();
});

test('BUBBLE_CONTAINER_DIMENSIONS', t => {
    const actual = TYPES.BUBBLE_CONTAINER_DIMENSIONS === 'bubble.container.dimensions',
        expect = true;
    t.ok(actual, expect);
    t.end();
});

after('end test ---------------------------------------', t => {
    t.end();
});

