import test from 'tape';
import mock from 'mock';
import { TYPES } from 'tests/fixtures/shared';
import { response, responseArtistReleases } from 'tests/fixtures/model';

const before = test;
const after = test;

const actions = mock('scripts/actions', {
    'scripts/constants': {
        'HEADER_TITLE': TYPES.HEADER_TITLE,
        'FETCH_DATA': TYPES.FETCH_DATA,
        'USER_COLLECTION': TYPES.USER_COLLECTION,
        'ARTIST_RELEASE': TYPES.ARTIST_RELEASE,
        'RATING_SELECTED': TYPES.RATING_SELECTED,
        'ARTIST_SELECTED': TYPES.ARTIST_SELECTED,
        'BUBBLE_CONTAINER_DIMENSIONS': TYPES.BUBBLE_CONTAINER_DIMENSIONS
    }
}, require);

before('description: actions', t => {
    t.end();
});

test('headerTitle', t => {
    const actual = actions.headerTitle('my app'),
        expect = {
            type: TYPES.HEADER_TITLE,
            payload: 'my app'
        };
    t.deepEqual(actual, expect);
    t.end();
});

test('fetchData', t => {
    const actual = actions.fetchData(true),
        expect = {
            type: TYPES.FETCH_DATA,
            payload: true
        };
    t.deepEqual(actual, expect);
    t.end();
});

test('userCollection', t => {
    const actual = actions.userCollection(response),
        expect = {
            type: TYPES.USER_COLLECTION,
            payload: response
        };
    t.deepEqual(actual, expect);
    t.end();
});

test('artistRelease', t => {
    const actual = actions.artistRelease(responseArtistReleases),
        expect = {
            type: TYPES.ARTIST_RELEASE,
            payload: responseArtistReleases
        };
    t.deepEqual(actual, expect);
    t.end();
});

test('ratingSelected', t => {
    const actual = actions.ratingSelected(3),
        expect = {
            type: TYPES.RATING_SELECTED,
            payload: 3
        };
    t.deepEqual(actual, expect);
    t.end();
});

test('artistSelected', t => {
    const actual = actions.artistSelected({
            id: 151223,
            name: 'Amy Winehouse'
        }),
        expect = {
            type: TYPES.ARTIST_SELECTED,
            payload: {
                id: 151223,
                name: 'Amy Winehouse'
            }
        };
    t.deepEqual(actual, expect);
    t.end();
});

test('bubbleContainer', t => {
    const actual = actions.bubbleContainer({
            dimension: 600,
            viewBoxDimensions: { w: 500, h: 600 },
            scaleTipFactor: []
        }),
        expect = {
            type: TYPES.BUBBLE_CONTAINER_DIMENSIONS,
            payload: {
                dimension: 600,
                viewBoxDimensions: { w: 500, h: 600 },
                scaleTipFactor: []
            }
        };
    t.deepEqual(actual, expect);
    t.end();
});

after('end test ---------------------------------------', t => {
    t.end();
});
