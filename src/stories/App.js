import React from 'react';
import { storiesOf, action } from '@kadira/storybook';

import {
    nodes,
    nodeArtist,
    scaleFactor,
    userCollectionartistListMock,
    artistsReleaseListMock
} from './fixtures';

import Header from '../scripts/components/header';
import BubblesChart from '../scripts/components/bubble-chart';
import RatingSelection from '../scripts/components/rating-selection';
import ArtistRelease from '../scripts/components/artist-release';
import ArtistList from '../scripts/components/artists-list';

const header = Header(React);
const bubblesChart = BubblesChart(React);
const ratingSelection = RatingSelection(React);
const artistRelease = ArtistRelease(React);
const artistList = ArtistList(React);

const $ = React.createElement;

storiesOf('HEADER ...', module)
    .add('header', () => {
        return (
            $('div', { className: 'container' }, $(header, { title: 'Today my music is...' }))
        );
    });

storiesOf('BUBBLES CONTAINER', module)
    .add('user collection', () => {

        const props = {
            w: 500,
            h: 600,
            bubbles: nodes,
            clickListener: action('clickme: click event was pressed once'),
            scaleTip: scaleFactor,
            createStars: true
        };

        return (
            $('div', { className: 'container' }, $(bubblesChart, props))
        );
    })
    .add('artist release', () => {

        const props = {
            w: 500,
            h: 600,
            bubbles: nodeArtist,
            clickListener: action('clickme: click event was pressed once'),
            createStars: false
        };

        return (
            $('div', { className: 'container' }, $(bubblesChart, props))
        );
    });

storiesOf('RATING COLLECTION', module)
    .add('selection', () => {
        return (
            $('div', { className: 'container' }, $(ratingSelection, {
                selected: 3,
                clickListener: action('clickme: click event was pressed once')
            }))
        );
    });


storiesOf('ARTIST RELEASE', module)
    .add('artist', () => {
        return (
            $('div', { className: 'container' }, $(artistRelease, {
                title: 'Amy Winehouse',
                clickListener: action('clickme: click event was pressed once')
            }))
        );
    });

storiesOf('ARTIST LIST', module)
    .add('user collection artist List', () => {
        return (
            $('div', { className: 'container' }, $(artistList, {
                list: userCollectionartistListMock,
                artistClickListener: action('clickme: click event was pressed once')
            }))
        );
    })

.add('artist release List', () => {
    return (
        $('div', { className: 'container' }, $(artistList, {
            list: artistsReleaseListMock,
            artistClickListener: action('clickme: click event was pressed once')
        }))
    );
});
