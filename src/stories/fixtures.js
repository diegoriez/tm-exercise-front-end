export const nodes = [{
    'value': 5,
    'name': 'At The BBC',
    'id': 4631508,
    'x': 120.20811145864707,
    'y': 182.44131444087063,
    'r': 56.22670250371179
}, {
    'value': 4,
    'name': 'EDR002',
    'id': 8386456,
    'x': 228.7547088370963,
    'y': 182.44131444087063,
    'r': 51.32772215902526
}, {
    'value': 3,
    'name': 'Donna',
    'id': 8678179,
    'x': 179.02527427815588,
    'y': 97.73073945434535,
    'r': 45.908910351121094
}, {
    'value': 2,
    'name': 'Room With View',
    'id': 4124289,
    'x': 92.76453402672152,
    'y': 89.42830889244145,
    'r': 39.75828262413324
}, {
    'value': 1,
    'name': 'The Princess - One',
    'id': 8783439,
    'x': 40.718114042932086,
    'y': 140.91915530093226,
    'r': 32.46250182616301
}, {
    'value': 0,
    'name': 'RA.EX080 Lindstrøm',
    'id': 7852150,
    'x': 41.42820241182855,
    'y': 197.3238154806271,
    'r': 22.954455175560547
}];

export const nodeArtist = [
    {
        'value': 2006,
        'name': 'Back To B...',
        'id': 51331,
        'x': 233.98823327808685,
        'y': 273.9200666890396,
        'r': 45.55566178542892
    },
    {
        'value': 2006,
        'name': 'You Know ...',
        'id': 51313,
        'x': 326.09088760590765,
        'y': 273.9200666890396,
        'r': 45.55566178542892
    },
    {
        'value': 2006,
        'name': 'Rehab',
        'id': 51311,
        'x': 280.03956044199725,
        'y': 194.15682828516998,
        'r': 45.55566178542892
    },
    {
        'value': 2006,
        'name': 'Back To B...',
        'id': 51256,
        'x': 187.93690611417648,
        'y': 194.15682828516998,
        'r': 45.55566178542892
    },
    {
        'value': 2006,
        'name': 'Back To B...',
        'id': 2342941,
        'x': 141.8855789502661,
        'y': 273.9200666890396,
        'r': 45.55566178542892
    },
    {
        'value': 2006,
        'name': 'Back To B...',
        'id': 3676725,
        'x': 187.9369061141765,
        'y': 353.6833050929091,
        'r': 45.55566178542892
    },
    {
        'value': 2004,
        'name': 'In My Bed',
        'id': 51330,
        'x': 280.01685546473846,
        'y': 353.6701963681768,
        'r': 45.53295774126497
    },
    {
        'value': 2004,
        'name': 'Pumps',
        'id': 51338,
        'x': 372.07409797798385,
        'y': 353.6963890417157,
        'r': 45.53295774126497
    },
    {
        'value': 2004,
        'name': 'In My Bed...',
        'id': 827046,
        'x': 418.1707921807987,
        'y': 274.0118148207863,
        'r': 45.53295774126497
    },
    {
        'value': 2004,
        'name': 'Love Song...',
        'id': 4089345,
        'x': 372.23298427640316,
        'y': 194.23553770698834,
        'r': 45.53295774126497
    },
    {
        'value': 2004,
        'name': 'Help Your...',
        'id': 5568001,
        'x': 326.22698688733715,
        'y': 114.49856516955192,
        'r': 45.53295774126497
    },
    {
        'value': 2003,
        'name': 'Stronger ...',
        'id': 51301,
        'x': 234.18126678093745,
        'y': 114.32173007374149,
        'r': 45.521601472790245
    },
    {
        'value': 2003,
        'name': 'Frank (5 ...',
        'id': 327862,
        'x': 142.14674138210626,
        'y': 114.28263459445222,
        'r': 45.521601472790245
    },
    {
        'value': 2003,
        'name': 'Take The ...',
        'id': 51307,
        'x': 95.86887161140453,
        'y': 193.83585051518403,
        'r': 45.521601472790245
    },
    {
        'value': 2003,
        'name': 'Frank',
        'id': 51290,
        'x': 49.8178515507266,
        'y': 273.5205977709522,
        'r': 45.521601472790245
    }
];

export const scaleFactor = [
    0.9499496616128044,
    0.867181169047141,
    0.7756307075138749,
    0.671715899780078,
    0.4854075535383827,
    0.46511627906976744
];


export const userCollectionartistListMock = [{
    'displayArtistLink': true,
    'picUrl': 'https://api-img.discogs.com/gdPp7S9a7vBkjzY31dvgirsmEos=/fit-in/150x150/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-4631508-1370493660-3930.jpeg.jpg',
    'artists': [{
        'id': 151223,
        'name': 'Amy Winehouse'
    }],
    'title': 'At The BBC',
    'added': '2016-07-23',
    'rating': 5
}, {
    'displayArtistLink': true,
    'picUrl': 'https://api-img.discogs.com/qqT8aJMkJnFpWXWMI_EKrM1UQXI=/fit-in/150x150/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-8386456-1463944669-1872.jpeg.jpg',
    'artists': [{
        'id': 4232982,
        'name': 'J. Albert'
    }, {
        'id': 3787553,
        'name': 'Person Of Interest'
    }],
    'title': 'EDR002',
    'added': '2016-07-23',
    'rating': 4
}, {
    'displayArtistLink': true,
    'picUrl': 'https://api-img.discogs.com/KJT_zjNs648Fd-prhrBvwAfxs1U=/fit-in/150x150/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-8678179-1466440779-3981.jpeg.jpg',
    'artists': [{
        'id': 47174,
        'name': 'Cassy'
    }],
    'title': 'Donna',
    'added': '2016-07-23',
    'rating': 3
}, {
    'displayArtistLink': true,
    'picUrl': 'https://api-img.discogs.com/kM2Hx-qx4oBmJUmnOgPLiIXXlc4=/fit-in/150x150/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-4124289-1356101610-8647.jpeg.jpg',
    'artists': [{
        'id': 1237316,
        'name': 'Joris Delacroix'
    }],
    'title': 'Room With View',
    'added': '2016-07-23',
    'rating': 2
}, {
    'displayArtistLink': true,
    'picUrl': 'https://api-img.discogs.com/nnltO-mkDiGJhDALxunTWMILyb0=/fit-in/150x150/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-8783439-1468686583-6467.jpeg.jpg',
    'artists': [{
        'id': 183523,
        'name': 'Parov Stelar'
    }],
    'title': 'The Princess - One',
    'added': '2016-07-23',
    'rating': 1
}, {
    'displayArtistLink': true,
    'picUrl': 'https://api-img.discogs.com/bZsDzaP4LU6j0_gIL1F1d2t4qng=/fit-in/150x150/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-7852150-1450177770-6630.jpeg.jpg',
    'artists': [{
        'id': 152312,
        'name': 'Lindstrøm'
    }],
    'title': 'RA.EX080 Lindstrøm',
    'added': '2016-07-23',
    'rating': 0
}];


export const artistsReleaseListMock = [{
    'displayArtistLink': false,
    'artists': [{
        'id': 2237469,
        'name': 'Cassy'
    }],
    'picUrl': 'https://api-img.discogs.com/dKPVa8wFfP7sV5lrAmWq4I4VrUk=/fit-in/90x90/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-2237469-1450021659-1957.jpeg.jpg',
    'title': 'Cassy 3',
    'added': 2010,
    'rating': 0
}, {
    'displayArtistLink': false,
    'artists': [{
        'id': 209786,
        'name': 'Cassy'
    }],
    'picUrl': 'https://api-img.discogs.com/Xku2XggYaUMLq-oxqWz8nUt4Iss=/fit-in/90x90/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-2058046-1261421407.jpeg.jpg',
    'title': 'In The Mix - Simply Devotion',
    'added': 2009,
    'rating': 0
}, {
    'displayArtistLink': false,
    'artists': [{
        'id': 678783,
        'name': 'Cassy'
    }],
    'picUrl': 'https://api-img.discogs.com/GDy97TqK_lhvxb8Iv3UjoskMRFY=/fit-in/90x90/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-1364389-1444983323-8119.jpeg.jpg',
    'title': 'Cassy 02',
    'added': 2008,
    'rating': 0
}, {
    'displayArtistLink': false,
    'artists': [{
        'id': 1295623,
        'name': 'Cassy'
    }],
    'picUrl': 'https://api-img.discogs.com/SwZBtkKHXq840oi34EyLEM9hzZI=/fit-in/90x90/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-1295623-1219845916.jpeg.jpg',
    'title': 'A Poem For You',
    'added': 2008,
    'rating': 0
}, {
    'displayArtistLink': false,
    'artists': [{
        'id': 82917,
        'name': 'Cassy'
    }],
    'picUrl': 'https://api-img.discogs.com/l1f7J9wKVPQf3BR3MkZ8yxuKX7o=/fit-in/90x90/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-1097827-1194181430.jpeg.jpg',
    'title': 'Nil Desperandum',
    'added': 2007,
    'rating': 0
}, {
    'displayArtistLink': false,
    'artists': [{
        'id': 974002,
        'name': 'Cassy / A Guy Called Gerald'
    }],
    'picUrl': 'https://api-img.discogs.com/t9y2nBdGrRunYSkrefQPEfZdSCQ=/fit-in/90x90/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-942223-1195132413.jpeg.jpg',
    'title': 'Somelightuntothenight / Bodecka',
    'added': 2007,
    'rating': 0
}, {
    'displayArtistLink': false,
    'artists': [{
        'id': 82913,
        'name': 'Dima (6), Cassy'
    }],
    'picUrl': 'https://api-img.discogs.com/VoiUrMfohhZ_HcibJly7sE1FH0k=/fit-in/90x90/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-1129911-1344791037-9271.jpeg.jpg',
    'title': '999Pebbles / Somelightuntothenight (Troy Pierce Close The Windows Remix)',
    'added': 2007,
    'rating': 0
}, {
    'displayArtistLink': false,
    'artists': [{
        'id': 876499,
        'name': 'Cassy'
    }],
    'picUrl': 'https://api-img.discogs.com/TvJ6W2TnhQ1ZD4nqThV-YeRKwDc=/fit-in/90x90/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-772763-1160459966.jpeg.jpg',
    'title': 'Panorama Bar 01',
    'added': 2006,
    'rating': 0
}, {
    'displayArtistLink': false,
    'artists': [{
        'id': 713282,
        'name': 'Cassy'
    }],
    'picUrl': 'https://api-img.discogs.com/DK-Im3KPHg_oM62FGmuwDBLt8JA=/fit-in/90x90/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-713282-1158689709.jpeg.jpg',
    'title': 'Cassy 1',
    'added': 2006,
    'rating': 0
}, {
    'displayArtistLink': false,
    'artists': [{
        'id': 2459082,
        'name': 'Cassy'
    }],
    'picUrl': 'https://api-img.discogs.com/kyXu89Q287LXCS2tSX0js0TaJhA=/fit-in/90x90/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-2459082-1285221018.jpeg.jpg',
    'title': 'RA.026',
    'added': 2006,
    'rating': 0
}, {
    'displayArtistLink': false,
    'artists': [{
        'id': 565172,
        'name': 'Cassy'
    }],
    'picUrl': 'https://api-img.discogs.com/70zSENvsHc6Jj1UO0fL44-E2A7s=/fit-in/90x90/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-565172-1134583831.jpeg.jpg',
    'title': 'My Auntie',
    'added': 2005,
    'rating': 0
}, {
    'displayArtistLink': false,
    'artists': [{
        'id': 387404,
        'name': 'Tracks And The City Feat. Cassy'
    }],
    'picUrl': 'https://api-img.discogs.com/ZKrdGaduimESul8TZKavRgdYua8=/fit-in/90x90/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-387404-1109854505.jpg.jpg',
    'title': "I'll Be There",
    'added': 2005,
    'rating': 0
}, {
    'displayArtistLink': false,
    'artists': [{
        'id': 130993,
        'name': 'Cassy'
    }],
    'picUrl': 'https://api-img.discogs.com/y3yqUO0m3rZMEdw8JxRY8lSOUJs=/fit-in/90x90/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-130993-001.jpg.jpg',
    'title': "Can't Turn Around",
    'added': 2003,
    'rating': 0
}, {
    'displayArtistLink': false,
    'artists': [{
        'id': 129200,
        'name': 'Zombie Nation Feat. Cassy Britton*'
    }],
    'picUrl': 'https://api-img.discogs.com/mKrprM8jeEIKDhfjADkAX73b1xw=/fit-in/90x90/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-178176-1210614996.jpeg.jpg',
    'title': 'Unload',
    'added': 2002,
    'rating': 0
}, {
    'displayArtistLink': false,
    'artists': [{
        'id': 182812,
        'name': 'Cassy + Dave The Hustler'
    }],
    'picUrl': 'https://api-img.discogs.com/t0wf-hz13VnVbgsrPlrANEIZEZY=/fit-in/90x90/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-182812-1149963466.jpeg.jpg',
    'title': 'Her Dream',
    'added': 2002,
    'rating': 0
}];
