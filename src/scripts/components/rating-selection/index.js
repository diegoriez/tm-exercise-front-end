import styles from './styles.css';

export default React => {

    const { func, number } = React.PropTypes,

        ratingSelectionComp = ({ selected = 0, clickListener }) => {

            const doSelected = (i, sel) => {

                if (i === 0 && sel === 0 ) {
                    return 'isSelected';
                }

                if ( i >= 1 && sel >= i ) {
                    return 'isSelected';
                }

                return '';
            };

            const doStars = (callback, numElms = 0) => Array(numElms).fill(true).map((item, idx) =>
              (<button className={ doSelected(idx, selected) } value={ idx } onClick={ callback } key={ idx }><i></i></button>));

            return (<div className={ `${ styles.container } rating-selection`}>
                      { doStars(clickListener, 6) }
                    </div>);
        };

    ratingSelectionComp.propTypes = {
        selected: number,
        clickListener: func.isRequired
    };

    return ratingSelectionComp;
};
