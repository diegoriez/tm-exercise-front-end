import styles from './styles.css';

export default React => {

    const {
        array,
        func,
        number,
        bool
    } = React.PropTypes,

        bubbleChartComp = ({ w = 0, h = 0, bubbles = [], clickListener, scaleTip = [], createStars}) => {

            const doCreateStars = value => {

                // fill the stars with a plain color  || outline
                const doFill = (starNum, current) => starNum > current ? styles.starFilled : styles.starOutline;

                return [0, 20, 40, 60, 80].map((item, idx) => (
                    <path key={idx} d="m0.5,6.3059l6.1115,0l1.8885,-5.8059l1.8885,5.8059l6.1115,0l-4.9443,3.5882l1.8886,5.8059l-4.9443,-3.5883l-4.9443,3.5883l1.8886,-5.8059l-4.9443,-3.5882z" className={ doFill(value, idx) } strokeWidth="1" stroke="#000" transform={`translate(${item}, 0)`}></path>
                ));
            };

            const doDiscGroup = elms => {

                const doFill = value => {
                        const values = ['starOutline', 'oneStar', 'twoStars', 'threeStars', 'fourStars', 'fiveStars' ];
                        return `${styles[values[value]]} ${createStars ? styles.bubbleCenter : 'center-stroke'}`;
                    },

                    setScaleFactor = (arr, idx) => (arr.length === 0) ? 1 : arr[idx];


                return elms.map((node, idx) => (

                  <g id={node.id} className={createStars ? styles.pointer : ''} key={ idx } onClick={ clickListener } transform={`translate(${node.x},${node.y})`}>
                    <circle fill="black" overflow="hidden" cx="0" cy="0" r={ node.r }></circle>
                    <circle strokeWidth="3" className={ doFill(node.value) } cx="0" cy="0" r={ Math.floor(node.r / 2) }></circle>
                    <circle fill="white" cx="0" cy="0" r={ Math.floor(node.r / 12) }></circle>
                    <circle cx="0" cy="0" r={ Math.floor(node.r / 1.1) } fill="black" fillOpacity={ createStars ? 0.5 : 0}></circle>
                    <g className="bubble-tip" transform={`scale(${setScaleFactor(scaleTip, idx)}) translate(0, -20)`}>
                      <g transform="translate(-50, 25)">
                        { createStars ? doCreateStars(node.value) : <div></div> }
                      </g>
                       <text textAnchor="middle" dy=".3em">{ node.name }</text>
                    </g>
                  </g>));

            };

            return (
          <div className="bubbles-chart">
            <svg viewBox={`0,0,${w},${h}`}>
              <g id="bubbles-container" className={ styles.container }>
              { doDiscGroup(bubbles) }
              </g>
            </svg>
          </div>);
        };

    bubbleChartComp.propTypes = {
        w: number,
        h: number,
        scaleTip: array,
        bubbles: array,
        clickListener: func,
        createStars: bool.isRequired
    };

    return bubbleChartComp;
};
