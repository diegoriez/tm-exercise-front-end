import createHeader from 'scripts/containers/header';
import { bubbleChartContainer } from 'scripts/containers/bubble-chart';
import createArtistRelease from 'scripts/containers/artist-release';
import createRatingSelection from 'scripts/containers/rating-selection';
import { artitsListContainter } from 'scripts/containers/artists-list';

export default React => {

    const Header = createHeader(React),
        BubbleChart = bubbleChartContainer(React),
        ArtistRelease = createArtistRelease(React),
        RatingSelection = createRatingSelection(React),
        ArtistsList = artitsListContainter(React);

    const {
        bool
    } = React.PropTypes,

        App = ({byUserCollection}) => {

            return (<div className="container">
                    <Header/>
                    <BubbleChart/>
                    {
                      byUserCollection ? <RatingSelection/> : <ArtistRelease/>
                    }
                    <ArtistsList/>
                  </div>);
        };

    App.propTypes = {
        byUserCollection: bool.isRequired
    };

    return App;
};
