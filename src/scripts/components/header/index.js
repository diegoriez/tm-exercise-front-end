export default React => {

    const {
        string
    } = React.PropTypes,

        headerComp = ({ title }) => (
            <header className="app-header">
             <h2>{title}</h2>
             </header>
          );

    headerComp.propTypes = {
        title: string.isRequired
    };

    return headerComp;
};

