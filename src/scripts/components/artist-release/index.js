import styles from './styles.css';

export default React => {
    const {
        func,
        string,
    } = React.PropTypes,

        artistReleaseComp = ({ clickListener, title }) => {

            return (
                <div className={ `${styles.container} artist-release` }>
                  <button onClick={ clickListener }><i></i></button>
                  <p>{title}</p>
                </div>
              );
        };

    artistReleaseComp.propTypes = {
        title: string.isRequired,
        clickListener: func.isRequired
    };

    return artistReleaseComp;
};
