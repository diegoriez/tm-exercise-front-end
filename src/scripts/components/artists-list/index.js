import styles from './styles.css';

export default React => {

    const {
        array
    } = React.PropTypes,

        artistListComp = ({ list = [], artistClickListener }) => {

            const doList = ls => {
                return ls.map((item, idx) => {
                    const {
                        title,
                        added,
                        rating,
                        picUrl,
                        id,
                        name,
                        displayArtistLink
                    } = item;

                    return (
                      <li data-artist-id={id} data-artist-name={name} className={displayArtistLink ? styles.pointer : ''} key={idx} onClick={ displayArtistLink ? artistClickListener : () => {} }>
                        <div className={ styles.pic } style={{'backgroundImage': `url("${picUrl}")`}}></div>
                          <div className={ `${styles.artists} artists-list_artists` }>
                            <h3>{ name }</h3>
                            <h4>{ title }</h4>
                          </div>
                          <div className={ styles.artistMetadata }>
                            <div>{ Array(rating).fill(true).map((i, indx) => <i key={indx}></i> ) }</div>
                            { added ? <time dateTime={ added }>{ added }</time> : ''}
                          </div>
                      </li>
                    );
                });
            };

            return (<ul className={`${styles.container} artists-list`}>{ doList(list )}</ul>);
        };

    artistListComp.PropTypes = {
        list: array.isRequired
    };

    return artistListComp;
};
