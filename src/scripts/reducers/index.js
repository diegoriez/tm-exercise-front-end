import { combineReducers } from 'redux';
import {
    HEADER_TITLE,
    FETCH_DATA,
    USER_COLLECTION,
    ARTIST_RELEASE,
    RATING_SELECTED,
    ARTIST_SELECTED,
    BUBBLE_CONTAINER_DIMENSIONS
} from 'scripts/constants';

export const releaseData = (state = {}, action) => {

    const actions = {

            [USER_COLLECTION]: () => ({
                releaseType: action.type,
                data: action.payload.releases.slice(0)
                    .sort((a, b) => b.rating - a.rating)
                    // .reverse()
            }),

            [ARTIST_RELEASE]: () => ({
                releaseType: action.type,
                data: action.payload.data.releases.slice(0)
                    .sort((a, b) => b.year - a.year)

            }),

            DEFAULT: () => state
        },

        reduce = actions[action.type] || actions.DEFAULT;

    return reduce();
};

export const headerTitle = (state = {}, action) =>
    action.type === HEADER_TITLE ? action.payload : state;

export const ratingSelected = (state = {}, action) =>
    action.type === RATING_SELECTED ? action.payload : state;

export const artistSelected = (state = {}, action) =>
    action.type === ARTIST_SELECTED ? action.payload : state;

export const fetchData = (state = {}, action) =>
    action.type === FETCH_DATA ? action.payload : state;

export const bubblesContainer = (state = {}, action) =>
    action.type === BUBBLE_CONTAINER_DIMENSIONS ? Object.assign({}, action.payload) : state;

export const rootReducer = combineReducers({
    headerTitle,
    fetchData,
    releaseData,
    ratingSelected,
    artistSelected,
    bubblesContainer
});
