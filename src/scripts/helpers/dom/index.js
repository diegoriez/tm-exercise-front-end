import d3 from 'd3';

export const getBubbleContainerDimensions = () => {
    const bounding = document.querySelector('.container').getBoundingClientRect();
    return Math.min(bounding.width, bounding.height);
};

export const getViewBoxDimensions = () => {
    const bounding = document.querySelector('.container').getBoundingClientRect();
    return {
        w: bounding.width,
        h: bounding.height
    };
};

export const getBubblesNodes = (data, dimension) => {

    const bubble = d3.layout.pack()
        .sort(null)
        .size([dimension, dimension])
        .padding(1),

        nodes = bubble.nodes({
            children: data
        }).filter(function(d) {
            return !d.children;
        });

    return nodes.map((item) => ({
        r: item.r,
        value: item.value,
        x: item.x,
        y: item.y
    }));
};

export const getBubblesTipsScaleFactor = () => {

    const bubblesContainer = document.querySelector('#bubbles-container'),
        bubbles = bubblesContainer.children,
        slice = [].slice;

    return slice.call(bubbles).map(item => {
        const bubbleWidth = item.getBoundingClientRect().width / 2.15,
            bubbleTipWidth = item.querySelector('.bubble-tip').getBoundingClientRect().width,
            scaleFactor = bubbleWidth / bubbleTipWidth;
        return isNaN(scaleFactor) ? 1 : scaleFactor;
    });
};

