import 'babel-polyfill';
import fetch from 'isomorphic-fetch';

export const fetchData = url => {
    return fetch(url, {
        method: 'GET'
    })
        .then(resp => resp.json())
        .catch(err => console.log('error: ', err.message));
};


export const fetchArtistReleases = id => fetchData(`api/artist-releases/${id}`);
export const fetchUserCollection = id => fetchData('api/user-collection');
