import { connect } from 'react-redux';
import { USER_COLLECTION } from 'scripts/constants';
import App from 'scripts/components/app';

const mapStateToProps = state => ({
    byUserCollection: state.releaseData.releaseType === USER_COLLECTION
});

export default React => connect(
    mapStateToProps,
    null,
    null, { pure: true }
)(App(React));
