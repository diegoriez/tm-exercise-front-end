import { connect } from 'react-redux';
import BubbleChart from 'scripts/components/bubble-chart';
import { store } from 'scripts';
import { fetchArtistReleases } from 'scripts/helpers/fetch';
import { getBubblesNodes } from 'scripts/helpers/dom';
import { cropText } from 'scripts/helpers/helper';
import { USER_COLLECTION } from 'scripts/constants';
import { fetchData, artistSelected, artistRelease } from 'scripts/actions';


export const getValuesForNodes = (values = [], predicate) =>
    values.map(item => ({
        value: (item[predicate] * 1) + 1
    }));


export const filterUserCollectionByRating = (data, filterValue = 0) => {
    const dofilter = () => {
            return data.filter(item => {
                return (item.rating * 1) === (filterValue * 1) ? item : '';
            });
        },
        action = (filterValue * 1) === 0 ? () => data : dofilter;
    return action();
};

export const collectionByUserBubbles = (data, nodes, filter, rating) => {
    return filter(data, rating).map((item, idx) => {
        const node = nodes[idx];
        return {
            value: item.rating,
            name: item.basic_information.title,
            id: item.id,
            x: node.x,
            y: node.y,
            r: node.r
        };
    });
};

export const releasesByArtistBubbles = (value = [], nodes) => {
    return value.map((item, idx) => {
        const node = nodes[idx];
        return {
            value: item.year,
            name: cropText(item.title, 9),
            id: item.id,
            x: node.x,
            y: node.y,
            r: node.r
        };
    });
};

export const doBubbles = state => {

    const predicate = state.releaseData.releaseType === USER_COLLECTION ? 'rating' : 'year',

        nodes = getBubblesNodes(
            getValuesForNodes(state.releaseData.data, predicate),
            state.bubblesContainer.dimension
        ),

        action = state.releaseData.releaseType === USER_COLLECTION ? collectionByUserBubbles : releasesByArtistBubbles;

    return action(
        state.releaseData.data,
        nodes,
        filterUserCollectionByRating,
        state.ratingSelected);
};

export const getReleaseArtistList = (data, id) => {

    let releaseArtists = [];

    data.some(item => {
        if ((item.id * 1) === (id * 1)) {
            releaseArtists = item.basic_information.artists;
            return true;
        }
        return false;
    });

    return releaseArtists.map(artist => ({ id: artist.id, name: artist.name }));
};

const mapStateToProps = state =>
    ({
        w: state.bubblesContainer.viewBoxDimensions.w,
        h: state.bubblesContainer.viewBoxDimensions.h,
        scaleTip: state.releaseData.releaseType === USER_COLLECTION ? state.bubblesContainer.scaleTipFactor : [],
        bubbles: doBubbles(state),
        createStars: state.releaseData.releaseType === USER_COLLECTION
    });

const mapDispatchToProps = dispatch => ({

    clickListener(e) {

        const releaseData = store.getState().releaseData,

            getArtistRelease = () => {

                const id = e.currentTarget.id,
                    artist = getReleaseArtistList(releaseData.data, id)[0];

                dispatch(fetchData(true));

                fetchArtistReleases(artist.id)
                    .then(resp => {

                        dispatch(artistSelected({
                            id: artist.id,
                            name: artist.name
                        }));

                        return resp;
                    })
                    .then(response => {
                        dispatch(artistRelease(response));
                    }).then(() => {
                        dispatch(fetchData(false));
                    })
                    .catch(err => {
                        console.log(err.message);
                    });
            },

            action = releaseData.releaseType === USER_COLLECTION ? getArtistRelease : () => {};
        action();
    }
});

export const bubbleChartContainer = React => connect(
    mapStateToProps,
    mapDispatchToProps,
    null, { pure: true }
)(BubbleChart(React));
