import { connect } from 'react-redux';
import { USER_COLLECTION } from 'scripts/constants';
import { fetchArtistReleases } from 'scripts/helpers/fetch';
import { fetchData, artistSelected, artistRelease } from 'scripts/actions';
import ArtistsList from 'scripts/components/artists-list';


export const prepareDataForUserCollectionList = (data, ratingSelected) => {

    const doDateAdded = str => str.substr(0, str.search(/T/)),
        list = [];

    data.forEach(item => {

        const {
            date_added,
            rating,
            basic_information: { artists, title, thumb }
        } = item,

            addToList = () => {
                artists.forEach(artist => {
                    list.push({
                        added: doDateAdded(date_added),
                        id: artist.id,
                        displayArtistLink: true,
                        name: artist.name,
                        picUrl: thumb,
                        rating,
                        title
                    });
                });
            },

            action = (ratingSelected === 0 || ratingSelected === rating) ?
            addToList : () => {};

        action();
    });

    return list;
};

export const prepareDataForArtistReleaseList = data => {

    return data.map(item => {

        const {
            id,
            thumb,
            artist,
            title,
            year
        } = item;

        return ({
            added: year,
            id,
            displayArtistLink: false,
            name: artist,
            picUrl: thumb,
            rating: 0,
            title: title
        });

    });
};

export const preareData = releaseType =>
    releaseType === USER_COLLECTION ? prepareDataForUserCollectionList : prepareDataForArtistReleaseList;

const mapStateToProps = state => ({
    list: preareData(state.releaseData.releaseType)(state.releaseData.data, state.ratingSelected)
});


const mapDispatchToProps = dispatch => ({

    artistClickListener(e) {

        e.preventDefault();

        const artistId = e.currentTarget.dataset.artistId,
            artistName = e.currentTarget.dataset.artistName;

        dispatch(fetchData(true));


        fetchArtistReleases(artistId)
            .then(response => {
                dispatch(artistSelected({
                    id: artistId,
                    name: artistName
                }));

                return response;
            })
            .then(resp => {
                dispatch(artistRelease(resp));
            })
            .then(() => {
                dispatch(fetchData(false));
            })
            .catch(err => {
                console.log(err.message);
            });
    }
});


export const artitsListContainter = React => connect(
    mapStateToProps,
    mapDispatchToProps,
    null, { pure: true }
)(ArtistsList(React));
