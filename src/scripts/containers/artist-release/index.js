import { connect } from 'react-redux';
import { fetchData, ratingSelected, userCollection, artistSelected } from 'scripts/actions';
import { fetchUserCollection } from 'scripts/helpers/fetch';
import ArtistRelease from 'scripts/components/artist-release';


const mapStateToProps = state => ({
    title: state.artistSelected.name
});

var mapDispatchToProps = dispatch => ({

    clickListener() {

        dispatch(fetchData(true));

        fetchUserCollection()
            .then(resp => {
                dispatch(ratingSelected(0));
                dispatch(artistSelected({
                    id: '',
                    name: ''
                }));

                return resp;
            })
            .then(response => {
                dispatch(userCollection(response));
            })
            .then(() => {
                dispatch(fetchData(false));
            })
            .catch(err => {
                console.log(err.message);
            });
    }
});

export default React => connect(
    mapStateToProps,
    mapDispatchToProps,
    null, { pure: true }
)(ArtistRelease(React));
