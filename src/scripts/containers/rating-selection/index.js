import { connect } from 'react-redux';
import { ratingSelected } from 'scripts/actions';
import RatingSelection from 'scripts/components/rating-selection';


const mapStateToProps = state => ({
    selected: state.ratingSelected
});


const mapDispatchToProps = dispatch => ({
    clickListener(e) {
        dispatch(ratingSelected(e.currentTarget.value * 1));
    }
});

export default React => connect(
    mapStateToProps,
    mapDispatchToProps,
    null, { pure: true }
)(RatingSelection(React));
