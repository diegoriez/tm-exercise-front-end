import { connect } from 'react-redux';
import Header from 'scripts/components/header';

const mapStateToProps = state => ({
    title: state.headerTitle
});

export default React => connect(mapStateToProps, null, null, { pure: true })(Header(React));
