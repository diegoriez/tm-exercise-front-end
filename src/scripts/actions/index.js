import {
    HEADER_TITLE,
    FETCH_DATA,
    USER_COLLECTION,
    ARTIST_RELEASE,
    RATING_SELECTED,
    ARTIST_SELECTED,
    BUBBLE_CONTAINER_DIMENSIONS
} from 'scripts/constants';

export const artistRelease = payload => ({ type: ARTIST_RELEASE, payload });
export const fetchData = payload => ({ type: FETCH_DATA, payload });
export const headerTitle = payload => ({ type: HEADER_TITLE, payload });
export const ratingSelected = payload => ({ type: RATING_SELECTED, payload });
export const userCollection = payload => ({ type: USER_COLLECTION, payload });
export const artistSelected = payload => ({ type: ARTIST_SELECTED, payload });
export const bubbleContainer = payload => ({ type: BUBBLE_CONTAINER_DIMENSIONS, payload });
