 import React from 'react';
 import { render } from 'react-dom';
 import { Provider } from 'react-redux';
 import createStore from 'scripts/store';
 import createApp from 'scripts/containers/app';
 import { USER_COLLECTION } from 'scripts/constants';
 import { fetchData, bubbleContainer, userCollection } from 'scripts/actions';
 import { rootReducer } from 'scripts/reducers';
 import {
     getBubbleContainerDimensions,
     getViewBoxDimensions,
     getBubblesTipsScaleFactor
 } from 'scripts/helpers/dom';
 import { fetchUserCollection } from 'scripts/helpers/fetch';

 let unsubscribe = () => {};
 const App = createApp(React);
 const initialState = {

     headerTitle: ' Today my music is',
     fetchData: false,
     releaseData: {
         releaseType: '',
         data: []
     },
     ratingSelected: 0,
     artistSelected: {
         id: '',
         name: ''
     },
     bubblesContainer: {
         dimension: 400,
         viewBoxDimensions: { w: 500, h: 300 },
         scaleTipFactor: []
     }
 };


 export const store = createStore(initialState, rootReducer);

 const renderApp = st => {

     render(<Provider store = { st }>
         <App/>
         </Provider>,
         document.querySelector('#root')
     );
 };

 const dimensionsForBubblesContainer = (st, bubbleContainerAction) => {
     // should get dimensions from DOM
     st.dispatch(bubbleContainerAction({
         dimension: getBubbleContainerDimensions(),
         viewBoxDimensions: getViewBoxDimensions(),
         scaleTipFactor: []
     }));
 };

 const fetchInitialData = (fetcher, fetchDataAction, st, userCollectionAction) => {

     st.dispatch(fetchDataAction(true));

     fetcher()
         .then(resp => {
             st.dispatch(userCollectionAction(resp));
         })
         .then(() => {
             st.dispatch(fetchDataAction(false));
         })
         .catch((err) => {
             console.log('---> ', err.message);
         });
 };

 const subscribe = () => {

     const scaleFactorForBubblesTip = (st, bubbleContainerAction, unsubs) => () => {

         const state = st.getState(),
             isUserCollection = state.releaseData.releaseType === USER_COLLECTION,
             isFetched = !state.fetchData,
             isEmpty = state.bubblesContainer.scaleTipFactor.length === 0;


         if ((isEmpty && isUserCollection && isFetched)) {

             st.dispatch(bubbleContainerAction({
                 dimension: state.bubblesContainer.dimension,
                 viewBoxDimensions: state.bubblesContainer.viewBoxDimensions,
                 scaleTipFactor: getBubblesTipsScaleFactor()
             }));

             unsubs();
         }
     };

     unsubscribe = store.subscribe(scaleFactorForBubblesTip(store, bubbleContainer, unsubscribe));
 };


 // -------------------------------------
 renderApp(store);
 dimensionsForBubblesContainer(store, bubbleContainer);
 subscribe();
 fetchInitialData(fetchUserCollection, fetchData, store, userCollection);
