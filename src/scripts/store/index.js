import { createStore } from 'redux';

export default (initialState = {}, rootReducer) =>
  createStore(rootReducer, initialState);

