export const HEADER_TITLE = 'header.title';
export const FETCH_DATA = 'fetch.data';
export const USER_COLLECTION = 'by.user.collection';
export const ARTIST_RELEASE = 'by.artist.release';
export const RATING_SELECTED = 'rating.selected';
export const ARTIST_SELECTED = 'artist.selected';
export const BUBBLE_CONTAINER_DIMENSIONS = 'bubble.container.dimensions';
